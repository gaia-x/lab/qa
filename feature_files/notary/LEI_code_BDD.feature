Feature: Validate LEI Codes 

  Background:
    Given the GLEIF API for LEI code validation is available

  Scenario Outline: Validate LEI code against the Global Legal Entity Identifier Foundation / GLEIF database
    Given the LEI code is "<LEI_CODE>"
    And the GLEIF API returns a "<Response>"

    When the API validates the LEI code
    Then the validation result should be "<validation_status>"
    And the result should provide evidence with the GLEIF API URL

    Examples:
      | LEI_CODE              | Response                | validation_status |
      | 529900W18LQJJN6SJ336  | ValidResponse           | Valid             |
      | 98765432109876543210  | NonExistingResponse     | Invalid           |
      | !$#@$@#%@#$D@_-!!!!   | ServerErrorResponse     | Invalid           |
