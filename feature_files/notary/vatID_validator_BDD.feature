Feature: VAT ID Validation

Background:
  Given the VAT ID validation service is available

  Scenario Outline: Validation of VAT ID
    Given The Notary Service API is up and running
    When a valid VAT ID "<VatId>" is provided
    Then the VAT ID should be validated
    And the validation result should indicate <Validation_Result>
    And the validation evidence should include the API URL

    Examples:
      | VatId           | Validation_Result |
      | FR87880851399   | valid            |
      | FR8788085138    | invalid          |
      | F878808513      | invalid          |
      | ""              | error            |
      | <empty or null> | error            |
