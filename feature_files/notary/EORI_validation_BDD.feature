Feature: EORI Validation

  Background:
    Given the Economic Operators Registration and Identification known as EORI validation API is configured with the European Commission API

  Scenario Outline: EORI validation with different responses
    Given an input of a given EORI number "<EORI>"
    And the European Commission API returns a "<Response>" response
    When the EORI is validated
    Then the result should be "<validation_Status>" with the country "<expectedCountry>"

    Examples:
      | EORI                     | Response     | validation_Status    | expectedCountry |
      | FR42279772000122         | valid        | valid                | France          |
      | FR42279772000122         | partial      | valid                |                 |
      | FR42279772WW0XZZ         | invalid      | invalid              |                 |
      | Hell0W@@!!!XTASS         | error        | invalid              |                 |
